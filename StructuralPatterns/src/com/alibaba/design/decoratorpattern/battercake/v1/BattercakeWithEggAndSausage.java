package com.alibaba.design.decoratorpattern.battercake.v1;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-22:15
 */
public class BattercakeWithEggAndSausage extends Battercake {
    @Override
    protected String getMsg() {
        return super.getMsg() + "+1根香肠";
    }

    @Override
    //加一个香肠加2块钱
    public int getPrice() {
        return super.getPrice() + 2;
    }
}
