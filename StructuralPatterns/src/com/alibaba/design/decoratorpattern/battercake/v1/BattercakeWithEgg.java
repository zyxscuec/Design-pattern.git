package com.alibaba.design.decoratorpattern.battercake.v1;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-22:14
 */
public class BattercakeWithEgg extends Battercake {
    @Override
    protected String getMsg() {
        return super.getMsg() + "+1个鸡蛋";
    }

    @Override
    //加一个鸡蛋加1块钱
    public int getPrice() {
        return super.getPrice() + 1;
    }
}
