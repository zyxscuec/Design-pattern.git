package com.alibaba.design.decoratorpattern.battercake.v1;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-22:13
 */
public class Battercake {

    protected String getMsg(){
        return "煎饼";
    }

    public int getPrice(){
        return 5;
    }
}
