package com.alibaba.design.decoratorpattern.battercake.v2;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-22:24
 */
public class BaseBattercake extends Battercake {
    @Override
    protected String getMsg(){
        return "煎饼";
    }

    @Override
    public int getPrice(){
        return 5;
    }
}
