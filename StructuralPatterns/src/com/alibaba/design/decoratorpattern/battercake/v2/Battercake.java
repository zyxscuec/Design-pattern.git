package com.alibaba.design.decoratorpattern.battercake.v2;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-22:23
 */
public abstract class Battercake {
    protected abstract String getMsg();
    protected abstract int getPrice();
}
