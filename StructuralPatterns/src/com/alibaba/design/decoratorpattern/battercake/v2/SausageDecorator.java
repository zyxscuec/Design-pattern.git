package com.alibaba.design.decoratorpattern.battercake.v2;

import java.io.InputStream;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-22:26
 */
public class SausageDecorator extends BattercakeDecorator {
    public SausageDecorator(Battercake battercake) {
        super(battercake);
    }

    @Override
    protected void doSomething() {

    }

    @Override
    protected String getMsg() {
        return super.getMsg() + "+1根香肠";
    }

    @Override
    protected int getPrice() {
        return super.getPrice() + 2;
    }

}
