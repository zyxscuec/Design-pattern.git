package com.alibaba.design.adapterpattern.loginadapter.v2.adapters;


import com.alibaba.design.adapterpattern.loginadapter.ResultMsg;

/**
 * Created by Tom on 2019/3/16.
 */
public interface RegistAdapter {
    boolean support(Object adapter);
    ResultMsg login(String id, Object adapter);
}
