package com.alibaba.design.adapterpattern.powerapapter;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-21:07
 */
public interface DC5 {
    public int outoupDC5V();
}
