package com.alibaba.design.adapterpattern.powerapapter;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-21:06
 */
public class AC220 {
    public int outputAC220V(){
        int output = 220;
        System.out.println("输出电流" + output + "V");
        return output;
    }
}
