package com.alibaba.design.adapterpattern.test;

import com.alibaba.design.adapterpattern.powerapapter.AC220;
import com.alibaba.design.adapterpattern.powerapapter.DC5;
import com.alibaba.design.adapterpattern.powerapapter.PowerAdapter;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-21:10
 */
public class PowerAdapterTest {

    public static void main(String[] args) {
        DC5 dc5 = new PowerAdapter(new AC220());
        dc5.outoupDC5V();
    }
}
