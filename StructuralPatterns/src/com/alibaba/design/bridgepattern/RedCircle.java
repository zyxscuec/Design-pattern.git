package com.alibaba.design.bridgepattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-17:38
 */
public class RedCircle implements DrawAPI {
    @Override
    public void drawCircle(int radius, int x, int y) {
        System.out.println("Drawing Circle[ color: red, radius: " + radius +", x: " +x+", "+ y +"]");
    }
}
