package com.alibaba.design.bridgepattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-17:37
 */
public interface DrawAPI {

    public void drawCircle(int radius, int x, int y);

}
