package com.alibaba.design.bridgepattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-17:39
 */
public abstract class Shape {

    protected DrawAPI drawAPI;
    protected Shape(DrawAPI drawAPI){
        this.drawAPI = drawAPI;
    }
    public abstract void draw();

}
