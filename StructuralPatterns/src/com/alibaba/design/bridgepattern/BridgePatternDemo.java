package com.alibaba.design.bridgepattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-17:40
 */
public class BridgePatternDemo {

    public static void main(String[] args) {
        Shape redCircle = new Circle(100,100, 10, new RedCircle());
        Shape greenCircle = new Circle(100,100, 10, new GreenCircle());

        redCircle.draw();
        greenCircle.draw();
    }

}
