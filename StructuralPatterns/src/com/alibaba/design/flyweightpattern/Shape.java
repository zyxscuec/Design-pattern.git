package com.alibaba.design.flyweightpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-18:48
 */
public interface Shape {

    void draw();
}
