package com.alibaba.design.flyweightpattern;

import java.util.HashMap;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-18:49
 */
public class ShapeFactory {

    private static final HashMap<String, Shape> circleMap = new HashMap<>();

    public static Shape getCircle(String color) {
        Circle circle = (Circle)circleMap.get(color);

        if(circle == null) {
            circle = new Circle(color);
            circleMap.put(color, circle);
            System.out.println("Creating circle of color : " + color);
        }
        return circle;
    }

}
