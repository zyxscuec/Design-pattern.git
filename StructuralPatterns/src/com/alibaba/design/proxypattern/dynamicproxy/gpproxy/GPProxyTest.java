package com.alibaba.design.proxypattern.dynamicproxy.gpproxy;


import com.alibaba.design.proxypattern.Person;
import com.alibaba.design.proxypattern.dynamicproxy.jdkproxy.Customer;

/**
 * Created by Tom on 2019/3/10.
 */
public class GPProxyTest {

    public static void main(String[] args) {
        try {

            //JDK动态代理的实现原理
            Person obj = (Person) new GPMeipo().getInstance(new Customer());
            System.out.println(obj.getClass());
            obj.findLove();

        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
