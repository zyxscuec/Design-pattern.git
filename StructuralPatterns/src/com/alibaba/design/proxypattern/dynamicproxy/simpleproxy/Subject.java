package com.alibaba.design.proxypattern.dynamicproxy.simpleproxy;

/**
 * Created by Tom.
 */
public interface Subject {
    void request();
}
