package com.alibaba.design.proxypattern.dynamicproxy.jdkproxy;

import com.alibaba.design.proxypattern.Person;
import sun.misc.ProxyGenerator;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-11:34
 */
public class Test {

    public static void main(String[] args) throws Exception {
        Person obj = (Person)new JDKMeipo().getInstance(new Customer());
        obj.findLove();

        byte [] bytes = ProxyGenerator.generateProxyClass("$Proxy0",new Class[]{Person.class});
        FileOutputStream os = new FileOutputStream("D://$Proxy0.class");
        os.write(bytes);
        os.close();

    }
}
