package com.alibaba.design.proxypattern.dynamicproxy.jdkproxy;

import com.alibaba.design.proxypattern.Person;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-11:11
 */
public class Customer implements Person {
    @Override
    public void findLove() {
        System.out.println("高富帅");
        System.out.println("身高180cm");
        System.out.println("有6块腹肌");
    }
}
