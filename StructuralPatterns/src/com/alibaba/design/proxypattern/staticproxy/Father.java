package com.alibaba.design.proxypattern.staticproxy;

import com.alibaba.design.proxypattern.Person;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-10:55
 */
public class Father implements Person {

    private Son son;

    public Father(Son son){
        this.son = son;
    }

    @Override
    public  void findLove(){
        System.out.println("父亲物色对象");
        this.son.findLove();
        System.out.println("双方父母同意，确立关系");
    }

    public void findJob(){

    }
}
