package com.alibaba.design.proxypattern.staticproxy;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-10:58
 */
public class FatherTest {

    public static void main(String[] args) {

        Father father = new Father(new Son());
        father.findLove();

        //农村，媒婆
        //婚介所

        //大家每天都在用的一种静态代理的形式
        //对数据库进行分库分表
        //ThreadLocal
        //进行数据源动态切换
    }
}
