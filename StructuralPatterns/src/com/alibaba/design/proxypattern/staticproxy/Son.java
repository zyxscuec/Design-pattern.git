package com.alibaba.design.proxypattern.staticproxy;

import com.alibaba.design.proxypattern.Person;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-10:55
 */
public class Son implements Person {
    @Override
    public void findLove() {
        System.out.println("儿子要求：肤白貌美大长腿");
    }
}
