package com.alibaba.design.proxypattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-10:54
 */
public interface Person {
    public void findLove();
}
