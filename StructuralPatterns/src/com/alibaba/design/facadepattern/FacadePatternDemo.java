package com.alibaba.design.facadepattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-18:20
 */
public class FacadePatternDemo {

    public static void main(String[] args) {
        ShapeMaker shapeMaker = new ShapeMaker();

        shapeMaker.drawCircle();
        shapeMaker.drawRectangle();
        shapeMaker.drawSquare();
    }

}
