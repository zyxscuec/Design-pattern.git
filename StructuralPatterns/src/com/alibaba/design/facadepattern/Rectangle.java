package com.alibaba.design.facadepattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-18:18
 */
public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("Rectangle::draw()");
    }
}
