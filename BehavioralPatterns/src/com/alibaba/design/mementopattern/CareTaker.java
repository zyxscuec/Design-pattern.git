package com.alibaba.design.mementopattern;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-22:50
 */
public class CareTaker {

    private List<Memento> mementoList = new ArrayList<Memento>();

    public void add(Memento state){
        mementoList.add(state);
    }

    public Memento get(int index){
        return mementoList.get(index);
    }

}
