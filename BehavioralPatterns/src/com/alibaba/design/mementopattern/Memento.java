package com.alibaba.design.mementopattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-22:49
 */
public class Memento {

    private String state;

    public Memento(String state){
        this.state = state;
    }

    public String getState(){
        return state;
    }

}
