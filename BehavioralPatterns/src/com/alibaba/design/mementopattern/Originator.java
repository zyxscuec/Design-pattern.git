package com.alibaba.design.mementopattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-22:50
 */
public class Originator {

    private String state;

    public void setState(String state){
        this.state = state;
    }

    public String getState(){
        return state;
    }

    public Memento saveStateToMemento(){
        return new Memento(state);
    }

    public void getStateFromMemento(Memento Memento){
        state = Memento.getState();
    }

}
