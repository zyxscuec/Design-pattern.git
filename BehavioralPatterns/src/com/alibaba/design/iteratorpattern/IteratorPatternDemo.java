package com.alibaba.design.iteratorpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-21:23
 */
public class IteratorPatternDemo {

    public static void main(String[] args) {
        NameRepository namesRepository = new NameRepository();

        for(Iterator iter = namesRepository.getIterator(); iter.hasNext();){
            String name = (String)iter.next();
            System.out.println("Name : " + name);
        }
    }

}
