package com.alibaba.design.iteratorpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-21:16
 */
public interface Container  {

    public Iterator getIterator();

}
