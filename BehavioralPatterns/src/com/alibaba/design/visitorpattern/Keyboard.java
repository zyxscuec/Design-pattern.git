package com.alibaba.design.visitorpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-8:55
 */
public class Keyboard implements ComputerPart {
    @Override
    public void accept(ComputerPartVisitor computerPartVisitor) {
        computerPartVisitor.visit(this);
    }
}
