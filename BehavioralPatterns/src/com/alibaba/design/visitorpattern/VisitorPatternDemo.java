package com.alibaba.design.visitorpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-9:08
 */
public class VisitorPatternDemo {

    public static void main(String[] args) {
        ComputerPart computer = new Computer();
        computer.accept(new ComputerPartDisplayVisitor());
    }

}
