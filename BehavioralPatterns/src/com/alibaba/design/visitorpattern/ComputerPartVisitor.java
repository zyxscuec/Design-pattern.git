package com.alibaba.design.visitorpattern;

import com.alibaba.design.visitorpattern.Mouse;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-8:54
 */
public interface ComputerPartVisitor {

    public void visit(Computer computer);
    public void visit(Mouse mouse);
    public void visit(Keyboard keyboard);
    public void visit(Monitor monitor);

}
