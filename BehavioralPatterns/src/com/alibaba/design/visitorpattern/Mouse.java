package com.alibaba.design.visitorpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-9:05
 */
public class Mouse implements ComputerPart {
    @Override
    public void accept(ComputerPartVisitor computerPartVisitor) {
        computerPartVisitor.visit(this);
    }
}
