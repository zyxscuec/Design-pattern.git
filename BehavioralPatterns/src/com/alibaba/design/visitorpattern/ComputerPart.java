package com.alibaba.design.visitorpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-8:54
 */
public interface ComputerPart {

    public void accept(ComputerPartVisitor computerPartVisitor);

}
