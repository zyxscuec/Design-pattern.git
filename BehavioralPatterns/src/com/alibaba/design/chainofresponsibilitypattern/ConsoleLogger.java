package com.alibaba.design.chainofresponsibilitypattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-21:50
 */
public class ConsoleLogger extends AbstractLogger {

    public ConsoleLogger(int level){
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("Standard Console::Logger: " + message);
    }
}
