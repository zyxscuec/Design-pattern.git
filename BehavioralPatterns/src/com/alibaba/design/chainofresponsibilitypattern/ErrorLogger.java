package com.alibaba.design.chainofresponsibilitypattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-21:51
 */
public class ErrorLogger extends AbstractLogger {

    public ErrorLogger(int level){
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("Error Console::Logger: " + message);
    }
}
