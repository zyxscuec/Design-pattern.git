package com.alibaba.design.mediatorpattern;



import java.util.Date;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-21:50
 */
public class ChatRoom {

    public static void showMessage(User user, String message){
        System.out.println(new Date().toString()
                + " [" + user.getName() +"] : " + message);
    }

}
