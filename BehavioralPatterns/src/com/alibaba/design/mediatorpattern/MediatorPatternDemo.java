package com.alibaba.design.mediatorpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-21:53
 */
public class MediatorPatternDemo {

    public static void main(String[] args) {
        User robert = new User("Robert");
        User john = new User("John");

        robert.sendMessage("Hi! John!");
        john.sendMessage("Hello! Robert!");
    }

}
