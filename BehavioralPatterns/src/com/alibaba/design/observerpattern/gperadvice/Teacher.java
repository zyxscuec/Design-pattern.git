package com.alibaba.design.observerpattern.gperadvice;

import java.util.Observable;
import java.util.Observer;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-9:43
 */
public class Teacher implements Observer {
    private String name;
    public Teacher(String name){
        this.name = name;
    }

    @Override
    public void update(Observable o, Object arg) {
        GPer gper = (GPer)o;
        Question question = (Question)arg;
        System.out.println("===============================");
        System.out.println(name + "老师，你好！\n" +
                "您收到了一个来自“" + gper.getName() + "”的提问，希望您解答，问题内容如下：\n" +
                question.getContent() + "\n" +
                "提问者：" + question.getUserName());
    }
}
