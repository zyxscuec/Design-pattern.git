package com.alibaba.design.observerpattern.gperadvice;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-9:43
 */
public class Question {
    private String userName;
    private String content;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
