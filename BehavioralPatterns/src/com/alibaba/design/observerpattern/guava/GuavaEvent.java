package com.alibaba.design.observerpattern.guava;

import com.google.common.eventbus.Subscribe;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-9:59
 */
public class GuavaEvent {

    @Subscribe
    public void subscribe(String str){
        System.out.println("执行subscribe方法，传入的参数是：" + str);
    }

}
