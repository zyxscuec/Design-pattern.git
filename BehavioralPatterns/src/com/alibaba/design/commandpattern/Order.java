package com.alibaba.design.commandpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-8:57
 */
public interface Order {

    void execute();

}
