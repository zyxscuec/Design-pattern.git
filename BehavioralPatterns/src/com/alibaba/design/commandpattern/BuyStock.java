package com.alibaba.design.commandpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-8:59
 */
public class BuyStock implements Order {

    private Stock abcStock;

    public BuyStock(Stock abcStock){
        this.abcStock = abcStock;
    }

    @Override
    public void execute() {
        abcStock.buy();
    }
}
