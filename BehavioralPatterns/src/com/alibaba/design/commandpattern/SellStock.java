package com.alibaba.design.commandpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-9:00
 */
public class SellStock implements Order {

    private Stock abcStock;

    public SellStock(Stock abcStock){
        this.abcStock = abcStock;
    }

    @Override
    public void execute() {
        abcStock.sell();
    }
}
