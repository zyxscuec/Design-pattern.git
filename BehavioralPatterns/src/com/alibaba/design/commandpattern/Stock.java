package com.alibaba.design.commandpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-8:57
 */
public class Stock {

    private String name = "Tom";

    private int quantity = 10;

    public void buy(){
        System.out.println("Stock [ Name: "+name+", Quantity: " + quantity +" ] bought");
    }

    public void sell(){
        System.out.println("Stock [ Name: "+name+", Quantity: " + quantity +" ] sold");
    }
}
