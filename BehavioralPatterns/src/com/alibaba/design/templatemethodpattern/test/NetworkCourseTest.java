package com.alibaba.design.templatemethodpattern.test;

import com.alibaba.design.templatemethodpattern.course.JavaCourse;
import com.alibaba.design.templatemethodpattern.course.NetworkCourse;
import com.alibaba.design.templatemethodpattern.course.PythonCourse;

import java.util.AbstractList;
import java.util.ArrayList;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-19:53
 */
public class NetworkCourseTest {

    public static void main(String[] args) {
        System.out.println("---Java架构师课程---");
        NetworkCourse javaCourse = new JavaCourse();
        javaCourse.createCourse();

        System.out.println("---Python课程---");
        NetworkCourse pythonCourse = new PythonCourse(true);
        pythonCourse.createCourse();

    }
}
