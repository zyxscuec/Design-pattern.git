package com.alibaba.design.templatemethodpattern.course;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-19:52
 */
public class PythonCourse extends NetworkCourse {
    private boolean needHomeworkFlag = false;

    public PythonCourse(boolean needHomeworkFlag){
        this.needHomeworkFlag = needHomeworkFlag;
    }

    @Override
    void checkHomeWork() {
        System.out.println("检查Python的作业");
    }

    @Override
    protected boolean needHomeWork() {
        return  this.needHomeworkFlag;
    }
}
