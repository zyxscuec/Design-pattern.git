package com.alibaba.design.templatemethodpattern.course;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-19:51
 */
public class JavaCourse extends NetworkCourse{
    @Override
    void checkHomeWork() {
        System.out.println("检查java的作业");
    }
}
