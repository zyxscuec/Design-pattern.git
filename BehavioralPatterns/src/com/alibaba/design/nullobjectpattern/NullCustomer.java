package com.alibaba.design.nullobjectpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-8:16
 */
public class NullCustomer extends AbstractCustomer {



    @Override
    public boolean isNil() {
        return true;
    }

    @Override
    public String getName() {
        return "Not Available in Customer Database";
    }
}
