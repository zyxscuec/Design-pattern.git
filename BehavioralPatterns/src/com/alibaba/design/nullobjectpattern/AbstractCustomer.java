package com.alibaba.design.nullobjectpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-8:14
 */
public abstract class AbstractCustomer {

    protected String name;

    public abstract boolean isNil();

    public abstract String getName();

}
