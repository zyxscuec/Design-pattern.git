package com.alibaba.design.nullobjectpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/2-8:15
 */
public class RealCustomer extends AbstractCustomer {

    public RealCustomer(String name) {
        this.name = name;
    }

    @Override
    public boolean isNil() {
        return false;
    }

    @Override
    public String getName() {
        return name;
    }
}
