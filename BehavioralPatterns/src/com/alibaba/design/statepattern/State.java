package com.alibaba.design.statepattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-23:24
 */
public interface State {

    public void doAction(Context context);

}
