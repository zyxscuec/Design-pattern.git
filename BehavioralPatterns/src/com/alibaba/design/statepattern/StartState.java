package com.alibaba.design.statepattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-23:25
 */
public class StartState implements State {

    public void doAction(Context context) {
        System.out.println("Player is in start state");
        context.setState(this);
    }

    @Override
    public String toString(){
        return "Start State";
    }
}
