package com.alibaba.design.statepattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-23:25
 */
public class Context {

    private State state;

    public Context(){
        state = null;
    }

    public void setState(State state){
        this.state = state;
    }

    public State getState(){
        return state;
    }

}
