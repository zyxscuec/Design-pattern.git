package com.alibaba.design.strategypattern.promotion;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-11:03
 */
public class CouponStrategy implements PromotionStrategy {
    @Override
    public void doPromotion() {
        System.out.println("通过优惠券的形式来促销");
    }
}
