package com.alibaba.design.strategypattern.promotion;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-11:05
 */
public class GroupbuyStrategy implements PromotionStrategy {
    @Override
    public void doPromotion() {
        System.out.println("通过拼团促销");
    }
}
