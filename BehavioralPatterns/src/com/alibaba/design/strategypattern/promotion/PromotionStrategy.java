package com.alibaba.design.strategypattern.promotion;

/**
 * 促销策略抽象
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-11:01
 */
public interface PromotionStrategy {
    public void doPromotion();
}
