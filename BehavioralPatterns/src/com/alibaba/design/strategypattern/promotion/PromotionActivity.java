package com.alibaba.design.strategypattern.promotion;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-11:06
 */
public class PromotionActivity {
    private PromotionStrategy promotionStrategy;

    public PromotionActivity(PromotionStrategy promotionStrategy){
        this.promotionStrategy = promotionStrategy;
    }

    public void execute(){
        promotionStrategy.doPromotion();
    }
}
