package com.alibaba.design.strategypattern.promotion;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-11:03
 */
public class CashbackStrategy implements PromotionStrategy {
    @Override
    public void doPromotion() {
        System.out.println("通过返现促销，消费额到达一定的额度后可以直接返现");
    }
}
