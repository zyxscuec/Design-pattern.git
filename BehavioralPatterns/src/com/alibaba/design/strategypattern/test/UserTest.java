package com.alibaba.design.strategypattern.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-17:02
 */
public class UserTest implements Comparator<User> {

    @Override
    public int compare(User o1, User o2) {
        return o1.score-o2.score;
    }

//    @Override
//    public int compare(User u1, User u2) {
//        return u1.name.compareTo(u2.name);
//    }

    public static void main(String[] args) {
        ArrayList<User> list = new ArrayList<>();
        list.add(new User("Tom",98));
        list.add(new User("Jerry",95));
        list.add(new User("Jane",99));
        list.add(new User("Mary",100));
        Collections.sort(list, new UserTest());
        for(User index:list) {
            System.out.println(" score: " + index.score + " name : " + index.name);
        }
    }
}
