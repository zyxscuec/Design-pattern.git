package com.alibaba.design.strategypattern.test;

import com.alibaba.design.strategypattern.promotion.CashbackStrategy;
import com.alibaba.design.strategypattern.promotion.CouponStrategy;
import com.alibaba.design.strategypattern.promotion.PromotionActivity;
import com.alibaba.design.strategypattern.promotion.PromotionStrategyFactory;
import org.apache.commons.lang3.StringUtils;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-11:23
 */
public class PromotionActivityTest {
    public static void main(String[] args) {
        System.out.println("============No.1===========");
        PromotionActivityTest.testPromotionActivity1();
        System.out.println("============No.2===========");
        PromotionActivityTest.testPromotionActivity2();
        System.out.println("============No.3===========");
        PromotionActivityTest.testPromotionActivity3();
    }

    public static void testPromotionActivity1(){
        PromotionActivity activity618 = new PromotionActivity(new CouponStrategy());
        PromotionActivity activity1111 = new PromotionActivity(new CashbackStrategy());

        activity618.execute();
        activity1111.execute();
    }

    public static void testPromotionActivity2() {
        PromotionActivity promotionActivity = null;

        String promotionKey = "COUPON";

        if (StringUtils.equals(promotionKey, "COUPON")) {
            promotionActivity = new PromotionActivity(new CouponStrategy());
        } else if (StringUtils.equals(promotionKey, "CASHBACK")) {
            promotionActivity = new PromotionActivity(new CashbackStrategy());
        }
        promotionActivity.execute();
    }

    public static void testPromotionActivity3() {
        String promotionKey = "GROUPBUY";
        PromotionActivity promotionActivity = new PromotionActivity(PromotionStrategyFactory.getPromotionStrategy(promotionKey));
        promotionActivity.execute();
    }

}
