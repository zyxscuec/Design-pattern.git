package com.alibaba.design.strategypattern.test;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/29-17:05
 */
public class User {

    public String name;

    public int score;

    public User(String name, int score) {
        this.name =name;
        this.score = score;
    }

}
