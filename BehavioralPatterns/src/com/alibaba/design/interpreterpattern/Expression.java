package com.alibaba.design.interpreterpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-10:10
 */
public interface Expression {
    public boolean interpret(String context);
}
