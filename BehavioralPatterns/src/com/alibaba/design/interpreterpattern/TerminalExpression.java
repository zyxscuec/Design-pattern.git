package com.alibaba.design.interpreterpattern;

/**
 * @author zhouyanxiang
 * @create 2020-08-2020/8/1-10:10
 */
public class TerminalExpression implements Expression {

    private String data;

    public TerminalExpression(String data){
        this.data = data;
    }

    @Override
    public boolean interpret(String context) {
        if(context.contains(data)){
            return true;
        }
        return false;
    }

}
