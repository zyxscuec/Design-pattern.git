package com.alibaba.design.singlepattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-15:51
 * 先静态后动态
 * 先属性后方法
 * 先上后下
 */
public class HungrySingleton {
    private static final HungrySingleton hungrySingleton = new HungrySingleton();

    private HungrySingleton(){

    }

    public static HungrySingleton getInstance(){
        return hungrySingleton;
    }
}
