package com.alibaba.design.singlepattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-9:15
 * 单例模式的实现非常简单：只由单个类组成
 * 为了确保单例实例的唯一性，所有单例的构造器都要被声明成私有的（private）
 * 再通过静态（static）方法实现全局访问获得该单例实例
 * 当我们在代码中使用单例对象的时候，只需要进行简单的调用即可
 * Singleton.getInstance().doSomething();
 */
public class Singleton {
    private static Singleton singleton;
    private Singleton(){
        System.out.println("Singleton is Instantiated");
    }

    public static Singleton getInstance(){
        if (singleton == null){
            singleton = new Singleton();
        }
        return singleton;
    }
    public void doSomething(){
        System.out.println("Something is done");
    }
}
