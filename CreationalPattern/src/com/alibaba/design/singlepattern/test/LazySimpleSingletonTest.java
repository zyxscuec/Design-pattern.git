package com.alibaba.design.singlepattern.test;

import com.alibaba.design.singlepattern.lazy.LazySimpleSingleton;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-16:22
 */
public class LazySimpleSingletonTest {

    public static void main(String[] args) {
        System.out.println("=============");
//        Thread t1 = new Thread(new ThreadSingletonTest());
//        Thread t2 = new Thread(new ThreadSingletonTest());
//        Thread t3 = new Thread(new ThreadSingletonTest());
//        Thread t4 = new Thread(new ThreadSingletonTest());
//        Thread t5 = new Thread(new ThreadSingletonTest());
//        t1.start();
//        t2.start();
//        t3.start();
//        t4.start();
//        t5.start();
        for (int i = 0; i < 100; i++) {
            Thread t = new Thread(new ThreadSingletonTest());
            t.start();
        }
        System.out.println("=============");
    }
}


