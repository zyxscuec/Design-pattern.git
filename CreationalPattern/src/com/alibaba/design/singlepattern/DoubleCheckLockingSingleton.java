package com.alibaba.design.singlepattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-9:30
 * 拥有双重校验锁机制的同步锁单例模式
 */
public class DoubleCheckLockingSingleton {
    private static volatile DoubleCheckLockingSingleton doubleCheckLockingSingleton;
    private DoubleCheckLockingSingleton(){
        System.out.println("Singleton is Instantiated");
    }

    /**
     * 双重检验
     * 执行过程：
     *     step1:先判断是否有student实例，如果有，返回实例
     *     step2:如果第一次执行这个方法，是没有实例的，这时进入if
     *     step3:通过synchronized块控制了同步，虽然会造成性能下降，但是只有第一次初始对象时会这样
     */
    public static  DoubleCheckLockingSingleton getInstance(){
        if (doubleCheckLockingSingleton == null){
            synchronized (DoubleCheckLockingSingleton.class){
                if (doubleCheckLockingSingleton == null){
                    doubleCheckLockingSingleton = new DoubleCheckLockingSingleton();
                }
            }

        }
        return doubleCheckLockingSingleton;
    }
    public void doSomething(){
        System.out.println("Something is done");
    }
}
