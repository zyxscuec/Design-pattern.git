package com.alibaba.design.singlepattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-9:39\
 * 无锁的线程安全单例模式
 */
public class LockFreeSingleton {
    private static final LockFreeSingleton LOCK_FREE_SINGLETON = new LockFreeSingleton();
    private LockFreeSingleton(){
        System.out.println("Singleton is Instantiated");
    }

    public static synchronized LockFreeSingleton getInstance(){
        return LOCK_FREE_SINGLETON;
    }
    public void doSomeThing(){
        System.out.println("Something is done");
    }
}
