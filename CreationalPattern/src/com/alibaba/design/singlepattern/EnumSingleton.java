package com.alibaba.design.singlepattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-21:37
 */
public enum  EnumSingleton {
    Instance;
    public void doSomeThing(){
        System.out.println("Something has been done");
    }
}
