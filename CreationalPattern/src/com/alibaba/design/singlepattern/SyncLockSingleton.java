package com.alibaba.design.singlepattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-9:22
 * 同步锁单例模式
 */
public class SyncLockSingleton {
    private static SyncLockSingleton syncLockSingleton;
    private SyncLockSingleton(){
        System.out.println("Singleton is Instantiated");
    }

    /**
     * 要么在方法前加上synchronized,要么使用synchronized代码块包装if (syncLockSingleton == null)
    */
    public static synchronized SyncLockSingleton getInstance(){
/*        synchronized (SyncLockSingleton.class){
            if (syncLockSingleton == null){
                syncLockSingleton = new SyncLockSingleton();
            }
        }*/
        if (syncLockSingleton == null){
            syncLockSingleton = new SyncLockSingleton();
        }
        return syncLockSingleton;
    }
    public void doSomething(){
        System.out.println("Something is done");
    }
}
