package com.alibaba.design.prototypepattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-15:33
 */
public class Square extends Shape {

    public Square() {
        type = "Square";
    }

    @Override
    void draw() {
        System.out.println("Inside Square::draw() method.");
    }
}
