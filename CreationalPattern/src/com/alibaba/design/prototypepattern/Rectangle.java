package com.alibaba.design.prototypepattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-15:32
 */
public class Rectangle extends Shape {

    public Rectangle(){
        type = "Rectangle";
    }

    @Override
    void draw() {
        System.out.println("Inside Rectangle::draw() method.");
    }
}
