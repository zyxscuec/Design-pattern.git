package com.alibaba.design.prototypepattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-15:34
 */
public class Circle extends Shape {
    public Circle(){
        type = "Circle";
    }

    @Override
    public void draw() {
        System.out.println("Inside Circle::draw() method.");
    }
}
