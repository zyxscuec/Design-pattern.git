package com.alibaba.design.factorypattern.abstractfactory;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-20:05
 */
public class ChinaCarFactory extends Car{
    @Override
    public void toTravel() {
        System.out.println("In China,I will choose car to travel");
    }
}
