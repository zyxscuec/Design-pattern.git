package com.alibaba.design.factorypattern.abstractfactory;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-20:08
 */
public class EnglandFactory implements VehicleFactory {
    @Override
    public Vehicle bikeToTravel() {
        return new EnglandBikeFactory();
    }

    @Override
    public Vehicle carToTravel() {
        return new EnglandCarFactory();
    }
}
