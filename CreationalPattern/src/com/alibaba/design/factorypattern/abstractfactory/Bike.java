package com.alibaba.design.factorypattern.abstractfactory;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-18:20
 */
public abstract class Bike implements Vehicle {
    public abstract void toTravel();
}
