package com.alibaba.design.factorypattern.abstractfactory;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-10:32
 */
public interface  Vehicle {
    void toTravel();
}
