package com.alibaba.design.factorypattern.abstractfactory;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-20:06
 */
public class EnglandCarFactory extends Car {
    @Override
    public void toTravel() {
        System.out.println("In England,I will choose car to travel");
    }
}
