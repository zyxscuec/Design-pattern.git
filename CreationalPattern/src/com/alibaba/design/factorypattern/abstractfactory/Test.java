package com.alibaba.design.factorypattern.abstractfactory;


/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-10:54
 */
public class Test {
    public static void main(String[] args) {
        // 创建中国工厂实例
        VehicleFactory chinaFactory = new ChinaFactory();
        // 通过中国工厂生产自行车实例
        Vehicle bikeToTravel = chinaFactory.bikeToTravel();
        // 在中国选择的出行方式
        bikeToTravel.toTravel();
        // 通过中国工厂生产汽车实例
        Vehicle carToTravel = chinaFactory.carToTravel();
        // 选择在中国的出行方式
        carToTravel.toTravel();

        System.out.println("=======================");

        // 创建英国工厂实例
        VehicleFactory englandFactory = new EnglandFactory();
        // 通过英国工厂生产自行车实例
        Vehicle  bikeToTravelEngland = englandFactory.bikeToTravel();
        // 在英国选择出行的方式
        bikeToTravelEngland.toTravel();
        // 通过英国工厂生产小汽车实例
        Vehicle  carToTravelEngland = englandFactory.carToTravel();
        // 在英国选择出行的方式
        carToTravelEngland.toTravel();
    }
}
