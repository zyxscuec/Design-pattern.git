package com.alibaba.design.factorypattern.abstractfactory;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-18:36
 */
public class EnglandBikeFactory extends Bike {
    @Override
    public void toTravel() {
        System.out.println("In England,I will Choose bike to travel");
    }
}
