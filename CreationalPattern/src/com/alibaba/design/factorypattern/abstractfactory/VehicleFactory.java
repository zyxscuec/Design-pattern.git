package com.alibaba.design.factorypattern.abstractfactory;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-10:25
 */
public interface  VehicleFactory {
    // 实例化单车出行模式
    public Vehicle bikeToTravel();
    // 实例化小汽车出行模式
    public Vehicle carToTravel();
}
