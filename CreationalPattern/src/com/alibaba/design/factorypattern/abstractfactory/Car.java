package com.alibaba.design.factorypattern.abstractfactory;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-18:19
 */
public abstract class Car implements Vehicle {
    public abstract void toTravel();
}
