package com.alibaba.design.factorypattern.abstractfactory;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-20:07
 */
public class ChinaFactory implements VehicleFactory {
    @Override
    public Vehicle bikeToTravel() {
        return new ChinaBikeFactory();
    }

    @Override
    public Vehicle carToTravel() {
        return new ChinaCarFactory();
    }
}
