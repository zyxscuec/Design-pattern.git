package com.alibaba.design.factorypattern.abstractfactory;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-10:55
 */
public class ChinaBikeFactory extends Bike {

    @Override
    public void toTravel() {
        System.out.println("In China,I will Choose bike to travel");
    }
}
