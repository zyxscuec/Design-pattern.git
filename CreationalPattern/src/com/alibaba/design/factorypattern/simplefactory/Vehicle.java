package com.alibaba.design.factorypattern.simplefactory;


/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-9:49
 */
public abstract class Vehicle {
    private String name;
    public Vehicle(String name) {
        this.name = name;
        System.out.println(name);
    }

    @Override
    public String toString(){
        return "Vehicle" + name;
    }

    abstract public Vehicle newInstance();


}
