package com.alibaba.design.factorypattern.simplefactory;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-10:06
 */
public class Bike extends Vehicle {
    public Bike(String name) {
        super(name);
    }

    @Override
    public Vehicle newInstance() {
        return new Bike("Bike ...");
    }
}
