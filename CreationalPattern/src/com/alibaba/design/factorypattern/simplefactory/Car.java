package com.alibaba.design.factorypattern.simplefactory;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-9:50
 */
public class Car  extends Vehicle {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Car(String name) {
        super(name);
        System.out.println(name);
    }

    @Override
    public String toString(){
        return "Car:" + name;
    }

    @Override
    public Vehicle newInstance() {
        return new Car("Car ...");
    }
}
