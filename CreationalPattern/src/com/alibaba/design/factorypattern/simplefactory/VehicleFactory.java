package com.alibaba.design.factorypattern.simplefactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-9:47
 */
public class VehicleFactory {
//    public enum VehicleType{
//        Bike,
//        Car,
//        Truck
//    }

//    public  static Vehicle create(VehicleType type){
//        if (type.equals(VehicleType.Bike)){
//            return new Bike("A Bike");
//        }else if (type.equals(VehicleType.Car)){
//            return new Car("A Car");
//        }else if (type.equals(VehicleType.Truck)){
//            return new Truck("A Truck");
//        }else {
//            return null;
//        }
//    }

    private Map<String,Vehicle> registeredProducts = new HashMap<>();

    public Vehicle createVehicle(String vehicleName){
        return registeredProducts.get(vehicleName).newInstance();
    }

    public void registerVehicle(String vehicleName,Vehicle vehicle){
        registeredProducts.put(vehicleName,vehicle);
    }
}
