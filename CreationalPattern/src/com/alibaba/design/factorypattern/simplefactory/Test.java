package com.alibaba.design.factorypattern.simplefactory;

import java.util.Calendar;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-9:54
 */
public class Test {
    public static void main(String[] args) {
        Vehicle vehicle = new Vehicle("A Car") {
            @Override
            public Vehicle newInstance() {
                return new Car(" audi ");
            }
        };
//        Vehicle vehicle2 = new Car("A Car");
//        System.out.println(vehicle.toString());
//        System.out.println(vehicle2.toString());
    }
}
