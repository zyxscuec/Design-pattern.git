package com.alibaba.design.factorypattern.simplefactory;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-10:07
 */
public class Truck extends Vehicle {
    public Truck(String name) {
        super(name);
    }

    @Override
    public Vehicle newInstance() {
        return new Truck("Truck ...");
    }
}
