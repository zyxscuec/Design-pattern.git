package com.alibaba.design.factorypattern.factorymothed;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-21:48
 */
public class CarFactory implements Factory {

    @Override
    public Vehicle toTravel() {
        return new Car();
    }
}
