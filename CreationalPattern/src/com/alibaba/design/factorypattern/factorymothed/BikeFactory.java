package com.alibaba.design.factorypattern.factorymothed;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-21:49
 */
public class BikeFactory implements Factory {

    @Override
    public Vehicle toTravel() {
        return new Bike();
    }
}
