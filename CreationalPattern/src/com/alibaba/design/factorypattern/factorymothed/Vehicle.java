package com.alibaba.design.factorypattern.factorymothed;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-21:43
 */
public interface Vehicle extends Factory{
    public void toTravelInfo();
}
