package com.alibaba.design.factorypattern.factorymothed;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-21:45
 */
public interface Factory {
    public Vehicle toTravel();
}
