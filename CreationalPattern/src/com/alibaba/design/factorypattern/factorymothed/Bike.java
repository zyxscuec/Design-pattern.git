package com.alibaba.design.factorypattern.factorymothed;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-21:54
 */
public class Bike extends BikeFactory implements Vehicle {

    @Override
    public void toTravelInfo() {
        System.out.println("Choose bike to travel");
    }
}
