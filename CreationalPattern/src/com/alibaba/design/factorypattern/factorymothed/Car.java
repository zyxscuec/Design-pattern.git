package com.alibaba.design.factorypattern.factorymothed;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-21:55
 */
public class Car extends CarFactory implements Vehicle {

    @Override
    public void toTravelInfo() {
        System.out.println("Choose car to travel ");
    }
}
