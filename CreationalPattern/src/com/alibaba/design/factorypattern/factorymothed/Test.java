package com.alibaba.design.factorypattern.factorymothed;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/30-21:57
 */
public class Test {

    public static void main(String[] args) {
        Factory bikeFactory = new BikeFactory();
        bikeFactory.toTravel().toTravelInfo();

        System.out.println("==================");

        Factory carFactory = new CarFactory();
        carFactory.toTravel().toTravelInfo();
    }
}
