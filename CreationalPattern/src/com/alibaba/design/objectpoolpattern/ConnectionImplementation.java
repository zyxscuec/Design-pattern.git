package com.alibaba.design.objectpoolpattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-19:14
 */
public class ConnectionImplementation implements Connection {
    @Override
    public Object get() {
        return new Object();
    }

    @Override
    public void set(Object x) {
        System.out.println("设置连接线程为： " + x);
    }
}
