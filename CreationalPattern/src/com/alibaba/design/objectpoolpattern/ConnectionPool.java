package com.alibaba.design.objectpoolpattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-19:15
 */
public class ConnectionPool {
    //池管理对象
    private static PoolManager pool = new PoolManager();

    //指定连接数 并添加
    public static void addConnections(int number) {
        for (int i = 0; i < number; i++) {
            pool.add(new ConnectionImplementation());
            System.out.println("添加第  " + i + " 个连接资源");
        }
    }
    //获取连接
    public static Connection getConnection() throws PoolManager.EmptyPoolException {
        return (Connection) pool.get();
    }
    //释放指定的连接
    public static void releaseConnection(Connection c) {
        pool.release(c);
        System.out.println("释放整个连接资源： " + c);
    }
}
