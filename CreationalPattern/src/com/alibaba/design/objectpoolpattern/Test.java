package com.alibaba.design.objectpoolpattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-19:16
 */
public class Test {
    static {
        ConnectionPool.addConnections(5);
    }

    public void test1() {
        Connection c;
        try {
            // 获得连接
            c = ConnectionPool.getConnection();
        } catch (PoolManager.EmptyPoolException e) {
            throw new RuntimeException(e);
        }
        // 设值
        c.set(new Object());
        //获取
        c.get();
        // 释放
        ConnectionPool.releaseConnection(c);
    }


    public static void main(String args[]) {
        Test test = new Test();
        test.test1();
    }

}
