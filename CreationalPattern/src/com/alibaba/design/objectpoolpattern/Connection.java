package com.alibaba.design.objectpoolpattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-19:14
 */
public interface Connection {
    Object get();

    void set(Object x);
}
