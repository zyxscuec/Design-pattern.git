package com.alibaba.design.builderpattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-10:45
 */
public interface Item {
    public String name();

    public Packing packing();

    public float price();
}
