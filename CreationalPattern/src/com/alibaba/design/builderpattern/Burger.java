package com.alibaba.design.builderpattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-10:47
 */
public abstract class Burger implements Item {
    public Packing packing(){
        return new Wrapper();
    }

    public abstract float price();
}
