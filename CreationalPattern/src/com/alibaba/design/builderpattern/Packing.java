package com.alibaba.design.builderpattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-10:46
 */
public interface Packing {
    public String pack();
}
