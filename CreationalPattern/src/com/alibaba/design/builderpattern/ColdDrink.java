package com.alibaba.design.builderpattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-10:48
 */
public abstract class ColdDrink implements Item {
    @Override
    public Packing packing() {
        return new Bottle();
    }

    @Override
    public abstract float price();
}