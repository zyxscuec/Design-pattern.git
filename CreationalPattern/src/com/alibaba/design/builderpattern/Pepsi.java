package com.alibaba.design.builderpattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-10:53
 */
public class Pepsi extends ColdDrink {

    @Override
    public String name() {
        return "Pepsi";
    }

    @Override
    public float price() {
        return 35.5f;
    }
}
