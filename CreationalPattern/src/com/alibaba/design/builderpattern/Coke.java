package com.alibaba.design.builderpattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-10:52
 */
public class Coke extends ColdDrink {
    @Override
    public String name() {
        return "Coke";
    }

    @Override
    public float price() {
        return 30.0f;
    }
}
