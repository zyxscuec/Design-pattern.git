package com.alibaba.design.builderpattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/31-10:47
 */
public class Bottle implements Packing {
    @Override
    public String pack() {
        return "Bottle";
    }
}
