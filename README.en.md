# Design-pattern

#### Description
Java中23种设计模式结合代码理解

# 设计模式

**所有的源码请参考** [https://gitee.com/zyxscuec/Design-pattern.git](https://gitee.com/zyxscuec/Design-pattern.git)

## 一、创造型设计模式

### 1.1 单例模式

单例模式（Singleton Pattern）是 Java 中最简单的设计模式之一。这种类型的设计模式属于创建型模式，它提供了一种创建对象的最佳方式。

这种模式涉及到一个单一的类，该类负责创建自己的对象，同时确保只有单个对象被创建。这个类提供了一种访问其唯一的对象的方式，可以直接访问，不需要实例化该类的对象。

单例模式的类结构图

![](https://gitee.com/zyxscuec/image/raw/master//img/20200728160601.png)

**注意：**

- 1、单例类只能有一个实例。
- 2、单例类必须自己创建自己的唯一实例。
- 3、单例类必须给所有其他对象提供这一实例。

这种很常见的应用在了spring的applicationContext.xml，在Servlet中的ServletContext、ServletConfig、

#### 1.1.1 饿汉式单例模式

Spring 中 IOC 容器 ApplicationContext 本身就是典型的饿汉式单例

饿汉式单例是在类加载的时候就立即初始化，并且创建单例对象。绝对线程安全，在线
程还没出现以前就是实例化了，不可能存在访问安全问题。

- 优点：没有加任何的锁、执行效率比较高，在用户体验上来说，比懒汉式更好。
- 缺点：类加载的时候就初始化，不管用与不用都占着空间，浪费了内存。

~~~Java
package com.alibaba.design.singlepattern;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-15:51
 * 先静态后动态
 * 先属性后方法
 * 先上后下
 */
public class HungrySingleton {
    private static final HungrySingleton hungrySingleton = new HungrySingleton();

    private HungrySingleton(){

    }

    public static HungrySingleton getInstance(){
        return hungrySingleton;
    }
}
~~~

或者也可以用下面这样，在静态代码块中去实例化。

~~~Java
package com.alibaba.design.singlepattern;

//饿汉式静态块单例
public class HungryStaticSingleton {
    private static final HungryStaticSingleton hungrySingleton;
    static {
        hungrySingleton = new HungryStaticSingleton();
    }
    private HungryStaticSingleton(){
        
    }
    public static HungryStaticSingleton getInstance(){
        return  hungrySingleton;
    }
}

~~~



#### 1.1.2 懒汉式单例模式

创建一个最简单的懒汉式单例模式，在不加synchronized的前提下是容易出现线程不安全的状况

~~~java
package com.alibaba.design.singlepattern.lazy;

/**
 * Created by Tom.
 */

//懒汉式单例
//在外部需要使用的时候才进行实例化
public class LazySimpleSingleton {
    private LazySimpleSingleton(){}
    //静态块，公共内存区域
    private static LazySimpleSingleton lazy = null;
    public /*synchronized*/ static LazySimpleSingleton getInstance(){
        if(lazy == null){
            lazy = new LazySimpleSingleton();
        }
        return lazy;
    }
}
~~~

创建线程池，开辟线程

~~~java
package com.alibaba.design.singlepattern.test;

import com.alibaba.design.singlepattern.lazy.LazySimpleSingleton;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-16:23
 */
public class ThreadSingletonTest implements  Runnable{

    @Override
    public void run() {
        LazySimpleSingleton singleton =  LazySimpleSingleton.getInstance();
        System.out.println(Thread.currentThread().getName() + " : " + singleton);
    }
}

~~~

测试懒汉式单例模式的线程

~~~Java
package com.alibaba.design.singlepattern.test;

import com.alibaba.design.singlepattern.lazy.LazySimpleSingleton;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-16:22
 */
public class LazySimpleSingletonTest {

    public static void main(String[] args) {
        System.out.println("=============");
        Thread t1 = new Thread(new ThreadSingletonTest());
        Thread t2 = new Thread(new ThreadSingletonTest());
        Thread t3 = new Thread(new ThreadSingletonTest());
        Thread t4 = new Thread(new ThreadSingletonTest());
        Thread t5 = new Thread(new ThreadSingletonTest());
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        System.out.println("=============");
    }
}

~~~

正常情况下应该是每次都会是相同的实例

![](https://gitee.com/zyxscuec/image/raw/master//img/20200728164210.png)

不过如果运行多次，也有一定的概率出现不同的实例

![](https://gitee.com/zyxscuec/image/raw/master//img/20200728163816.png)

如图所示，出现了多个不同的单例，由此可见在多线程的情况下是容易出现多个实例的，这样是不安全的。

为了解决这个不安全的状况，可以在实例化方法那里加上synchronized关键字

~~~Java
package com.alibaba.design.singlepattern.lazy;

/**
 * Created by Tom.
 */

//懒汉式单例
//在外部需要使用的时候才进行实例化
public class LazySimpleSingleton {
    private LazySimpleSingleton(){}
    //静态块，公共内存区域
    private static LazySimpleSingleton lazy = null;
    public synchronized static LazySimpleSingleton getInstance(){
        if(lazy == null){
            lazy = new LazySimpleSingleton();
        }
        return lazy;
    }
}
~~~



然后我开辟了100个线程随机测试，测试多次每次得到的实例对象都是同一个，解决了线程不安全的问题

~~~Java
package com.alibaba.design.singlepattern.test;

import com.alibaba.design.singlepattern.lazy.LazySimpleSingleton;

/**
 * @author zhouyanxiang
 * @create 2020-07-2020/7/28-16:22
 */
public class LazySimpleSingletonTest {

    public static void main(String[] args) {
        System.out.println("=============");
//        Thread t1 = new Thread(new ThreadSingletonTest());
//        Thread t2 = new Thread(new ThreadSingletonTest());
//        Thread t3 = new Thread(new ThreadSingletonTest());
//        Thread t4 = new Thread(new ThreadSingletonTest());
//        Thread t5 = new Thread(new ThreadSingletonTest());
//        t1.start();
//        t2.start();
//        t3.start();
//        t4.start();
//        t5.start();
        for (int i = 0; i < 100; i++) {
            Thread t = new Thread(new ThreadSingletonTest());
            t.start();
        }
        System.out.println("=============");
    }
}

~~~

